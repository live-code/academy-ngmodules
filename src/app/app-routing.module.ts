import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./features/login/login.component";
import {NgModule} from "@angular/core";

export const routes: Routes = [
  {
    path: 'shop',
    loadChildren: () => import('./features/shop/shop.module').then(m => m.ShopModule)
  },
  {path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
  {path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
  {path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule)},
  {path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  {path: '**', redirectTo: 'home'}
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
