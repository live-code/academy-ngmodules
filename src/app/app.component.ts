import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `

    <app-navbar></app-navbar>
    <app-card></app-card>

    <hr>
    <router-outlet></router-outlet>

  `,
  styles: []
})
export class AppComponent {
  title = 'academy-module';
}
