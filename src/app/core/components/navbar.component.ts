import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="shop">shop</button>
    <button routerLink="home">home</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="contacts">contacts</button>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
