import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import { TabbarComponent } from './layout/tabbar.component';
import {CardModule} from "./layout/card.module";
import {AccordionComponent} from "./layout/accordion.component";


@NgModule({
  declarations: [
    TabbarComponent,
    AccordionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CardModule,
  ],
  exports: [
    CardModule,
    FormsModule,
    AccordionComponent
  ]
})
export class SharedModule { }
