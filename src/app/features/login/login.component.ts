import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `

    <h1>Login</h1>

    <router-outlet></router-outlet>

    <hr>
    <button routerLink="signin">signin</button>
    <button routerLink="../login/registration">registration</button>
    <button routerLink="/login/lostpass">lost password</button>


  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
