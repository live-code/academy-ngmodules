import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from "./login.component";
import {SignInComponent} from "./components/sign-in.component";
import {RegistrationComponent} from "./components/registration.component";
import {LostPassComponent} from "./components/lost-pass.component";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    LoginComponent,
      SignInComponent,
      RegistrationComponent,
      LostPassComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SignInComponent},
          { path: 'lostpass', component: LostPassComponent},
          { path: 'registration', component: RegistrationComponent},
          { path: '', redirectTo: 'signin'}
        ]
      }
    ])
  ]
})
export class LoginModule { }
