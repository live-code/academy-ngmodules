import {NgModule} from "@angular/core";
import {ShopComponent} from "./shop.component";
import {ShopListComponent} from "./components/shop-list.component";
import {ShopOffersComponent} from "./components/shop-offers.component";
import {ShopSearchComponent} from "./components/shop-search.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    ShopComponent,
    ShopListComponent,
    ShopOffersComponent,
    ShopSearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      /*{path: 'offers', component: ShopOffersComponent },*/
      {path: '', component: ShopComponent }
    ])
  ],
  exports: [
  ]
})
export class ShopModule {}


