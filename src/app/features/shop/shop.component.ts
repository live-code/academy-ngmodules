import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop',
  template: `
    <h1>SHOP</h1>

    <app-shop-list></app-shop-list>
    <app-shop-offers></app-shop-offers>
    <app-shop-search></app-shop-search>
  `,
  styles: [
  ]
})
export class ShopComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
