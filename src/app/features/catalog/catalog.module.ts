import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import {FormsModule} from "@angular/forms";
import {CardComponent} from "../../shared/layout/card.component";
import {SharedModule} from "../../shared/shared.module";
import {OffersComponent} from "./modules/offers/offers.component";
import {CardModule} from "../../shared/layout/card.module";


const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      {
        path: 'offers',
        loadChildren: () => import('./modules/offers/offers.module').then(m => m.OffersModule)
      },
      {
        path: 'promo',
        loadChildren: () => import('./modules/promo/promo.module').then(m => m.PromoModule)
      },
      {
        path: '',
        redirectTo: 'offers'
      }
    ]
  },

];

@NgModule({
  declarations: [
    CatalogComponent,
  ],
  imports: [
    SharedModule,
    CardModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CatalogModule { }
