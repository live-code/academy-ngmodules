import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two',
  template: `
    <p>
      two works!
    </p>
  `,
  styles: [
  ]
})
export class TwoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
