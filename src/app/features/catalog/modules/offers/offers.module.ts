import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersComponent } from './offers.component';
import { OneComponent } from './components/one.component';
import { TwoComponent } from './components/two.component';
import {RouterModule} from "@angular/router";
import {CardModule} from "../../../../shared/layout/card.module";



@NgModule({
  declarations: [
    OffersComponent,
    OneComponent,
    TwoComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    RouterModule.forChild([
      { path: '', component: OffersComponent}
    ])
  ]
})
export class OffersModule { }
