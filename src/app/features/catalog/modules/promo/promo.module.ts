import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoComponent } from './promo.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    PromoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PromoComponent}
    ])
  ]
})
export class PromoModule { }
