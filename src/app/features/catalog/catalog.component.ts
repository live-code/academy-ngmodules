import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <h1>Catalog</h1>

    <button routerLink="offers">Offers</button>
    <button routerLink="promo">Promo</button>
    <button>xyz</button>

    <router-outlet></router-outlet>

  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {
  label = 'pippo'
  constructor() { }

  ngOnInit(): void {
  }

}
